"""
Copyright 2024 Ahnaf Haque Saihan <ahs.social@tutanota.com>
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, you can obtain one at http://mozilla.org/MPL/2.0/.
"""

import sys
import csv
from collections import defaultdict
from os import listdir
from PyQt6.QtWidgets import QApplication, QMainWindow, QTableWidgetItem
from Drugwidget import Ui_Rx


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_Rx()
        self.ui.setupUi(self)
        self.ui.Brand.setEditable(True)
        self.ui.Generic.setEditable(True)
        self.ui.Dose.setEditable(True)
        self.ui.TimeBox.setEditable(True)
        self.ui.Form.setEditable(True)
        self.datapath = "Data/"
        self.commit_data = {}
        druglist = [
            drugname[0:-4]
            for drugname in listdir(self.datapath)
            if drugname.endswith(".csv")
        ]
        self.DL = self.ui.DrugList
        self.DL.setRowCount(1)
        self.DL.setColumnCount(7)
        self.DL.setHorizontalHeaderLabels(
            [
                "Name of Drug",
                "Brand Name",
                "Form",
                "Dose",
                "Schedule",
                "Duration",
                "Comment",
            ]
        )
        self.ui.TimeBox.addItems(["days", "weeks", "months"])
        self.ui.Generic.addItems(druglist)
        self.ui.Generic.activated.connect(self.populate_brand)
        self.ui.Form.activated.connect(self.populate_dose)
        self.ui.Add.clicked.connect(self.add_drug)
        self.ui.Generate.clicked.connect(self.write_to_file)
        self.rowPosition = 0

    def populate_brand(self):
        generic_drug = self.ui.Generic.currentText()

        try:
            with open(self.datapath + generic_drug + ".csv") as csvfile:
                Reader = csv.DictReader(csvfile)
                self.csv_columns = defaultdict(list)
                # read a row as {column1: value1, column2: value2,...}
                for row in Reader:
                    for (
                        header,
                        entry,
                    ) in row.items():  # go over each column name and value
                        self.csv_columns[header].append(entry)
                self.ui.Brand.clear()
                self.ui.Brand.addItems(self.csv_columns["Name"])
                self.ui.Form.clear()
                self.ui.Form.addItems(list(self.csv_columns.keys())[1:])
                self.ui.Dose.clear()
        except Exception:
            self.ui.Form.activated.disconnect()

    def populate_dose(self):
        self.ui.Dose.clear()
        self.ui.Dose.addItems(self.csv_columns[self.ui.Form.currentText()])

    def add_drug(self):
        commit_data = [
            self.ui.Generic.currentText(),
            self.ui.Brand.currentText(),
            self.ui.Form.currentText(),
            self.ui.Dose.currentText(),
            self.ui.Schedule.text(),
            self.ui.Duration.text() + " " + self.ui.TimeBox.currentText(),
            self.ui.Comment.text(),
        ]
        for i in range(7):
            self.DL.setItem(self.rowPosition, i, QTableWidgetItem(commit_data[i]))
        self.rowPosition += 1
        self.DL.insertRow(self.rowPosition)
        self.ui.Brand.clear()
        self.ui.Dose.clear()
        self.ui.Schedule.clear()
        self.ui.Form.clear()
        self.ui.Duration.clear()
        self.ui.Comment.clear()

    def write_to_file(self):
        with open("Prescription.csv", "w") as output:
            writer = csv.writer(output)
            for j in range(self.rowPosition):
                row_to_list = []
                for k in range(7):
                    row_to_list.append(str(self.DL.item(j, k).text()))
                writer.writerow(row_to_list)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec())
